# GCstar_Viewer

Android application to browse collections imported from GCstar

## Usage

Copy your collections to a folder on your Android device (.gcs or .gcz files). GCstar Viewer will automatically find them.


## Support

Support by creating new issues in this repository or posting to the [GCstar forum](https://forums.gcstar.org/)

## Authors and acknowledgment

Original author : Tian

Current maintainer : Kerenoc

## License

LGPL v2 license

## Project status

The GCstar Viewer can still be found on some Android store but the current version is for old versions of Android. It may require some tweaking (emulation of hardware buttons). Volunteers to port it to current Android SKD are welcome (forks and merge requests).

