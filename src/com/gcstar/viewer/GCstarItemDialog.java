/****************************************************
 *
 *  Copyright 2010 Christian Jodar
 *
 *  This file is part of GCstar Viewer.
 *
 *  GCstar Viewer is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  GCstar Viewer is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GCstar Viewer; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************/

package com.gcstar.viewer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.Display;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Gallery;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.ViewFlipper;
import android.widget.TabHost.TabSpec;

public class GCstarItemDialog extends Dialog implements OnClickListener,
        OnTouchListener
{
    public GCstarItemDialog(Context context)
    {
        super(context, android.R.style.Theme);
        _parent = (GCstarViewer) context;
        _flipper = new ViewFlipper(context);
        setTitle("Item");
        resetImageHeight();

        _nextInAnimation = new TranslateAnimation(Animation.RELATIVE_TO_PARENT,
                +1.0f, Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        _nextInAnimation.setDuration(_animationTimer);
        _nextInAnimation.setInterpolator(new AccelerateInterpolator());

        _nextOutAnimation = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, -1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        _nextOutAnimation.setDuration(_animationTimer);
        _nextOutAnimation.setInterpolator(new AccelerateInterpolator());

        _prevInAnimation = new TranslateAnimation(Animation.RELATIVE_TO_PARENT,
                -1.0f, Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        _prevInAnimation.setDuration(_animationTimer);
        _prevInAnimation.setInterpolator(new AccelerateInterpolator());

        _prevOutAnimation = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, +1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        _prevOutAnimation.setDuration(_animationTimer);
        _prevOutAnimation.setInterpolator(new AccelerateInterpolator());

        _gestureDetector = new GestureDetector(new MyGestureDetector());
    }

    public void resetImageHeight()
    {
        Display display = ((WindowManager) _parent
                .getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        _imageHeight = display.getHeight() / 3;

        if (_images != null)
        {
            for (ImageButton image : _images)
            {
                image.setMaxHeight(_imageHeight);
            }
        }
        if (_coverView != null)
        {
            _coverView.getLayoutParams().height = _imageHeight;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        //_accListener = new AccelerometerListener(this);
    }

    @Override
    public void show()
    {
        //_accListener.start();
        super.show();
    }
    
    @Override
    public void dismiss()
    {
        //_accListener.stop();
        super.dismiss();
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = _parent.getMenuInflater();
        inflater.inflate(R.menu.item, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu)
    {
        boolean[] prevNext = _parent.areRelativesPossible();
        for (int i = 0; i < 2; i++)
        {
            menu.getItem(i).setEnabled(prevNext[i]);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    class MyGestureDetector extends SimpleOnGestureListener
    {
        public MyGestureDetector()
        {
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                float velocityY)
        {
            if ((e1 == null) || (e2 == null))
                return false;
            if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
            {
                return false;
            }
            else
            {
                try
                {
                    // right to left swipe
                    if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE
                            && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY)
                    {
                        return shiftItem(+1);
                    }
                    // left to right swipe
                    else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE
                            && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY)
                    {
                        return shiftItem(-1);
                    }
                }
                catch (Exception e)
                {
                    // nothing
                }
            }
            return false;
        }
    }

    public boolean onTouch(View arg0, MotionEvent ev)
    {
        //return false;
        return _gestureDetector.onTouchEvent(ev);
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.prev:
                shiftItem(-1);
                return true;
            case R.id.next:
                shiftItem(+1);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean shiftItem(int direction)
    {
        boolean[] prevNext = _parent.areRelativesPossible();
        int currentTab = _tabs.getCurrentTab();
        if (direction == -1)
        {
            if (!prevNext[0])
            {
                return false;
            }
            if (_withAnimation)
            {
                _flipper.setInAnimation(_prevInAnimation);
                _flipper.setOutAnimation(_prevOutAnimation);
                _flipper.addView(createTabs());
            }
            _parent.showRelativeItem(-1);
            if (_withAnimation)
            {
                _flipper.showPrevious();
                _flipper.removeViewAt(0);
            }
        }
        else
        {
            if (!prevNext[1])
            {
                return false;
            }
            if (_withAnimation)
            {
                _flipper.setInAnimation(_nextInAnimation);
                _flipper.setOutAnimation(_nextOutAnimation);
                _flipper.addView(createTabs());
            }
            _parent.showRelativeItem(+1);
            if (_withAnimation)
            {
                _flipper.showNext();
                _flipper.removeViewAt(0);
            }
        }
        _tabs.setCurrentTab(currentTab);
        _coverView.getLayoutParams().height = _imageHeight;
        return true;
    }

    public void setModel(GCstarModel model)
    {
        _model = model;
        _flipper.removeAllViews();
        _flipper.addView(createTabs());
        _flipper.setDisplayedChild(0);
        setContentView(_flipper);

    }

    public TabHost createTabs()
    {
        _tabs = new TabHost(getContext(), null);

        _tabWidget = new TabWidget(getContext())
        {
            public void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
            {
                super.onMeasure(widthMeasureSpec, heightMeasureSpec);
                _frameLayout.setPadding(0, getMeasuredHeight() + 3, 0, 0);
            }
        };
        _tabWidget.setId(android.R.id.tabs);
        _tabWidget.setLayoutParams(new TabWidget.LayoutParams(
                LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
        // tabWidget.setPadding(0,200,0,0);
        // TabWidget.LayoutParams tparams = new TabWidget.LayoutParams(
        // LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
        // tparams.gravity = Gravity.BOTTOM;
        // tabWidget.setLayoutParams(tparams);
        // tabWidget.getLayoutParams().height = 100;
        _tabs.addView(_tabWidget);

        _frameLayout = new FrameLayout(getContext());
        _frameLayout.setId(android.R.id.tabcontent);
        // _frameLayout.setPadding(0, 65, 0, 0);
        _tabs.addView(_frameLayout/*
                                   * , new LinearLayout.LayoutParams(
                                   * LayoutParams.WRAP_CONTENT,
                                   * LayoutParams.WRAP_CONTENT)
                                   */);

        _tabs.setup();

        _widgets = new HashMap<String, View>();
        _tables = new HashMap<String, ViewGroup>();
        _linears = new HashMap<String, ViewGroup>();
        _images = new ArrayList<ImageButton>();
        _lastCheckBoxPosition = 0;
        Iterator<String> git = _model.getGroups().iterator();
        while (git.hasNext())
        {
            String group = git.next();
            TabSpec t1 = _tabs.newTabSpec(group);
            t1.setIndicator(group);
            addTab(group);
            TabHost.TabContentFactory f = new TabHost.TabContentFactory()
            {
                public View createTabContent(String tag)
                {
                    GCstarItemDialog parent = GCstarItemDialog.this;
                    final ScrollView scroll = new ScrollView(parent
                            .getContext());
                    scroll.setOnTouchListener(GCstarItemDialog.this);
                    LinearLayout layout = new LinearLayout(parent.getContext());
                    layout
                            .setLayoutParams(new LinearLayout.LayoutParams(
                                    LayoutParams.FILL_PARENT,
                                    LayoutParams.FILL_PARENT));
                    layout.setOrientation(LinearLayout.VERTICAL);
                    layout.setGravity(Gravity.CENTER);
                    if (parent.isFirstTab(tag))
                    {
                        _coverView = parent.createImage(true);
                        layout.addView(_coverView);
                    }
                    layout.addView(parent.getTable(tag));
                    layout.addView(parent.getLinear(tag));
                    scroll.addView(layout);
                    return scroll;
                }
            };
            t1.setContent(f);
            _tabs.addTab(t1);

            Iterator<GCstarField> fit = _model.getFields(group).iterator();
            while (fit.hasNext())
            {
                GCstarField field = fit.next();
                if (field._name.equals(_model.getUrlField()))
                {
                    _urlLabel = _model.getLabel(field._label);
                }
                else
                {
                    addWidget(group, field);
                }
            }

            /*
             * View icon =
             * tabWidget.getChildAt(i).findViewById(android.R.id.icon);
             * ((ViewGroup)(tabWidget.getChildAt(i))).removeView(icon); i++;
             */
        }

        _urlField = _model.getUrlField();
        if (_urlField.length() != 0)
        {
            _urlButton = new Button(getContext());
            _widgets.put(_urlField, _urlButton);
            _urlButton.setText(_urlLabel);
            _urlButton.setOnClickListener(this);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.FILL_PARENT,
                    LinearLayout.LayoutParams.FILL_PARENT);
            params.setMargins(10, 20, 10, 10);
            _urlButton.setLayoutParams(params);

            _linears.get(_firstTab).addView(_urlButton);
        }

        _tabs.setId(123);
        return _tabs;
    }

    public void addWidget(String tabName, GCstarField field)
    {
        int type = field._type;
        switch (type)
        {
            case GCstarModel.Cover:
                break;
            case GCstarModel.Image:
            {
                ImageButton image = createImage(false);
                _widgets.put(field._name, image);
                TextView label = new TextView(getContext());
                label.setText(field._label);
                _linears.get(tabName).addView(label);
                _linears.get(tabName).addView(image);
                break;
            }
            case GCstarModel.YesNo:
            {
                TextView label = new TextView(getContext());
                label.setText(field._label);

                // CheckBox widget = new CheckBox(getContext());
                // ImageView widget = new ImageView(getContext());
                // LayoutParams layout = new
                // LayoutParams(LayoutParams.FILL_PARENT,
                // LayoutParams.FILL_PARENT);
                // widget.setLayoutParams(layout);

                TextView widget = new TextView(getContext());
                widget.setSingleLine(false);
                widget.setGravity(Gravity.LEFT);
                _widgets.put(field._name, widget);

                TableRow tr = new TableRow(getContext());
                tr.addView(label);
                tr.addView(widget);
                _tables.get(tabName).addView(tr, _lastCheckBoxPosition++);
                break;
            }
            case GCstarModel.MultiList:
            {
                TableLayout table = new TableLayout(getContext());
                TableLayout.LayoutParams tparams = new TableLayout.LayoutParams(
                        TableLayout.LayoutParams.FILL_PARENT,
                        TableLayout.LayoutParams.FILL_PARENT);
                tparams.setMargins(5, 5, 5, 5);
                table.setLayoutParams(tparams);

                _widgets.put(field._name, table);
                TextView label = new TextView(getContext());
                label.setText(field._label);

                TableRow tr = new TableRow(getContext());
                int i = 0;
                for (String header : field._labels)
                {
                    TextView headerView = new TextView(getContext());
                    headerView.setText(header);
                    tr.addView(headerView);
                    table.setColumnStretchable(i, true);
                    i++;
                }
                table.addView(tr, new TableLayout.LayoutParams(
                        LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));

                _linears.get(tabName).addView(label);
                _linears.get(tabName).addView(table);
                break;
            }
            default:
            {
                TextView label = new TextView(getContext());
                label.setText(field._label);

                TextView widget = new TextView(getContext());
                widget.setSingleLine(false);
                widget.setGravity(Gravity.LEFT);

                _widgets.put(field._name, widget);

                if ((type == GCstarModel.ShortText)
                        || (type == GCstarModel.Date))
                {
                    TableRow tr = new TableRow(getContext());
                    tr.addView(label);
                    tr.addView(widget);
                    _tables.get(tabName).addView(
                            tr,
                            new TableLayout.LayoutParams(
                                    LayoutParams.FILL_PARENT,
                                    LayoutParams.WRAP_CONTENT));
                }
                else
                {
                    _linears.get(tabName).addView(label);
                    _linears.get(tabName).addView(widget);
                }
                break;
            }
        }
    }

    public ImageButton createImage(boolean isCover)
    {
        ImageButton image = new ImageButton(getContext());
        image.setOnClickListener(this);
        // image.setMinimumHeight(_imageHeight);
        image.setMaxHeight(_imageHeight);
        // image.setPadding(2, 2, 2, 2);
        image.setAdjustViewBounds(true);
        image.setLayoutParams(new Gallery.LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
        _images.add(image);
        return image;
    }

    public void addTab(String name)
    {
        TableLayout table = new TableLayout(getContext());
        TableLayout.LayoutParams tparams = new TableLayout.LayoutParams(
                TableLayout.LayoutParams.FILL_PARENT,
                TableLayout.LayoutParams.FILL_PARENT);
        tparams.setMargins(5, 5, 5, 5);
        table.setLayoutParams(tparams);
        table.setColumnStretchable(0, true);
        table.setColumnShrinkable(0, false);
        table.setColumnStretchable(1, true);
        table.setColumnShrinkable(1, true);

        if (_tables.isEmpty())
        {
            _firstTab = name;
        }
        _tables.put(name, table);
        LinearLayout linear = new LinearLayout(getContext());
        LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(
                LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
        lparams.setMargins(5, 5, 5, 5);
        linear.setLayoutParams(lparams);
        linear.setOrientation(LinearLayout.VERTICAL);
        _linears.put(name, linear);
    }

    public boolean isFirstTab(String name)
    {
        return name.equals(_firstTab);
    }

    public View getTable(String name)
    {
        return _tables.get(name);
    }

    public View getLinear(String name)
    {
        return _linears.get(name);
    }

    public void setCurrentItem(GCstarItem item)
    {
        _parent.getLoader().cleanTempImages();
        setTitle(item.getTitle());
        Iterator<Map.Entry<String, String>> it = item.getValues().entrySet()
                .iterator();
        while (it.hasNext())
        {
            Map.Entry<String, String> pair = it.next();
            String key = pair.getKey();
            String value = pair.getValue();
            int type = _model.getFieldType(key);
            if (type == GCstarModel.Cover)
            {
                setImage(_coverView, value, true);
            }
            else
            {
                if (_widgets.containsKey(key))
                {
                    switch (type)
                    {
                        case GCstarModel.Image:
                            setImage((ImageButton) (_widgets.get(key)), value,
                                    false);
                            break;
                        /*
                         * case GCstarModel.YesNo:
                         * ((ImageView)_widgets.get(key)).setImageResource(
                         * value.equals("1") ? R.drawable.check_yes :
                         * R.drawable.check_no ); break;
                         */
                        case GCstarModel.Url:
                            setCurrentUrl(value);
                            break;
                        case GCstarModel.MultiList:
                            TableLayout table = (TableLayout) (_widgets
                                    .get(key));
                            table.removeViews(1, table.getChildCount() - 1);
                            ArrayList<ArrayList<String>> data = item
                                    .getList(key);
                            if (data != null)
                            {
                                for (ArrayList<String> line : data)
                                {
                                    TableRow tr = new TableRow(getContext());
                                    for (String col : line)
                                    {
                                        TextView headerView = new TextView(
                                                getContext());
                                        headerView.setText(col);
                                        tr.addView(headerView);
                                    }
                                    table.addView(tr,
                                            new TableLayout.LayoutParams(
                                                    LayoutParams.FILL_PARENT,
                                                    LayoutParams.WRAP_CONTENT));
                                }
                            }
                            break;
                        default:
                            ((TextView) (_widgets.get(key))).setText(value);
                    }
                }
            }
        }
    }

    public void onClick(View v)
    {
        if (v instanceof ImageButton)
        {
            Intent intent = new Intent();
            intent.setAction(android.content.Intent.ACTION_VIEW);
            CharSequence description = v.getContentDescription();
            if (description != null)
            {
                intent.setDataAndType(Uri.parse(description.toString()),
                        "image/*");
                _parent.startActivity(intent);
            }
        }
        else
        {
            if (_currentUrl.length() != 0)
            {
                Intent intent = new Intent();
                intent.setAction(android.content.Intent.ACTION_VIEW);
                intent.setData(Uri.parse(_currentUrl));
                _parent.startActivity(intent);
            }
        }
    }

    protected void setCurrentUrl(String url)
    {
        _currentUrl = url;
        String[] pieces = _currentUrl.split("##");
        if (pieces.length > 1)
        {
            _urlButton.setText(pieces[1]);
        }
        else
        {
            _urlButton.setText(_urlLabel);
        }
    }

    protected void setImage(ImageButton image, String path, boolean isCover)
    {
        // image.getLayoutParams().height = isCover ? _imageHeight :
        // LayoutParams.WRAP_CONTENT;

        image.setImageResource(android.R.drawable.ic_popup_sync);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 2;
        _parent.getLoader().loadImage(image, path, isCover, _imageHeight,
                options, false /* no cache */);
    }

    public class AccelerometerListener implements SensorEventListener
    {
        private SensorManager sensorManager;
        private List<Sensor> sensors;
        private Sensor sensor;
        private long lastUpdate = -1;
        private long currentTime = -1;
        private float last_x, last_y, last_z;
        private float current_x, current_y, current_z, currenForce;
        private static final int FORCE_THRESHOLD = 900;
        private final int DATA_X = SensorManager.DATA_X;
        private final int DATA_Y = SensorManager.DATA_Y;
        private final int DATA_Z = SensorManager.DATA_Z;
        private GCstarItemDialog subscriber;
        public AccelerometerListener(GCstarItemDialog parent)
        {
            SensorManager sensorService = (SensorManager) parent._parent.getSystemService(Context.SENSOR_SERVICE);
            this.sensorManager = sensorService;
            this.subscriber = parent;
            this.sensors = sensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER);
            if (sensors.size() > 0)
            {
                sensor = sensors.get(0);
            }
        }

        public void start()
        {
            if (sensor != null)
            {
                sensorManager.registerListener(this, sensor,
                        SensorManager.SENSOR_DELAY_GAME);
            }
        }

        public void stop()
        {
            sensorManager.unregisterListener(this);
        }

        public void onAccuracyChanged(Sensor s, int valu)
        {
        }

        public void onSensorChanged(SensorEvent event)
        {
            if (event.sensor.getType() != Sensor.TYPE_ACCELEROMETER
                    || event.values.length < 3)
                return;
            currentTime = System.currentTimeMillis();
            if ((currentTime - lastUpdate) > 100)
            {
                long diffTime = (currentTime - lastUpdate);
                lastUpdate = currentTime;
                current_x = event.values[DATA_X];
                current_y = event.values[DATA_Y];
                current_z = event.values[DATA_Z];
                currenForce = Math.abs(current_x + current_y + current_z
                        - last_x - last_y - last_z)
                        / diffTime * 10000;

                if (currenForce > FORCE_THRESHOLD)
                {
                    subscriber.shiftItem(+1);
                    /*
                    if (current_x > 0)
                    {
                        subscriber.shiftItem(+1);
                    }
                    else
                    {
                        subscriber.shiftItem(-1);
                    }*/
                }

                last_x = current_x;
                last_y = current_y;
                last_z = current_z;
            }
        }
    }

    private GCstarViewer _parent;
    private GCstarModel _model;
    private HashMap<String, View> _widgets;
    private ArrayList<ImageButton> _images;
    private HashMap<String, ViewGroup> _tables;
    private HashMap<String, ViewGroup> _linears;
    private ImageButton _coverView;
    private String _firstTab;
    public int _imageHeight;
    private Button _urlButton;
    private String _currentUrl;
    private String _urlField;
    private String _urlLabel;
    private int _lastCheckBoxPosition;
    private FrameLayout _frameLayout;
    private TabWidget _tabWidget;
    private ViewFlipper _flipper;
    private TranslateAnimation _nextInAnimation;
    private TranslateAnimation _nextOutAnimation;
    private TranslateAnimation _prevInAnimation;
    private TranslateAnimation _prevOutAnimation;
    private boolean _withAnimation = true;
    private int _animationTimer = 500;
    private static final int SWIPE_MIN_DISTANCE = 100;
    private static final int SWIPE_MAX_OFF_PATH = 250;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;
    private GestureDetector _gestureDetector;
    //private AccelerometerListener _accListener;
    private TabHost _tabs;
    
};
