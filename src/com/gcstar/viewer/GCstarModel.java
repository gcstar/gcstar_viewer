/****************************************************
*
*  Copyright 2010 Christian Jodar
*
*  This file is part of GCstar Viewer.
*
*  GCstar Viewer is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  GCstar Viewer is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with GCstar Viewer; if not, write to the Free Software
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
*****************************************************/

package com.gcstar.viewer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.content.res.Resources;
import android.content.res.XmlResourceParser;



public class GCstarModel
{
	public static final int None = 0;
	public static final int ShortText = 1;
	public static final int LongText = 2;
	public static final int Image = 3;
	public static final int Cover = 4;
	public static final int MultiList = 5;
	public static final int Date = 6;
	public static final int Url = 7;
	public static final int YesNo = 8;
		
	public GCstarModel(String name, GCstarViewer activity)
	{
		_titleField = new String();
		_groups = new ArrayList<String>();
		_fields = new HashMap<String, ArrayList<GCstarField>>();
		_fieldTypes = new HashMap<String, Integer>();
		_name = new String(name);
		_listFields = new HashSet<String>();
		_activity = activity;
		_hasLending = false;
		_hasTags = true;
		
		HashMap<String, String> groupNames = new HashMap<String, String>();
		
		Resources res = activity.getResources();
		int xmlId = res.getIdentifier(name, "xml", "com.gcstar.viewer");
		XmlResourceParser xpp = res.getXml(xmlId);
		try
		{
			xpp.next();
			int eventType = xpp.getEventType();
			boolean isTitle = false;
			boolean isCover = false;
			boolean isUrl = false;
			// Used to ignore all fields before title
			boolean gotTitle = false;
			boolean alreadyAdded = false;
			while (eventType != XmlPullParser.END_DOCUMENT)
			{
				if(eventType == XmlPullParser.START_TAG)
				{
					if (xpp.getName().equals("group"))
					{
						String id = xpp.getAttributeValue(null, "id");
						String label = getLabel(xpp.getAttributeValue(null, "label"));
						if (id != null)
						{
							groupNames.put(id, label);
						}
					}
					else if (xpp.getName().equals("fields"))
					{
						String lending = xpp.getAttributeValue(null, "lending");
						String tags = xpp.getAttributeValue(null, "tags");
						if ((lending != null) && (lending.equals("true")))
						{
							_hasLending = true;
						}
						if ((tags != null) && (tags.equals("true")))
						{
							_hasTags = true;
						}
					}
					else if (xpp.getName().equals("title"))
					{
						isTitle = true;
					}
					else if (xpp.getName().equals("cover"))
					{
						isCover = true;
					}
					else if (xpp.getName().equals("url"))
					{
						isUrl = true;
					}
					else if (xpp.getName().equals("field"))
					{
						alreadyAdded = false;
						String id = xpp.getAttributeValue(null, "value");
						String typeName = xpp.getAttributeValue(null, "type");
						String label = getLabel(xpp.getAttributeValue(null, "label"));
						String group = xpp.getAttributeValue(null, "group");
						if (group != null)
						{
							group = groupNames.get(group);
						}
						
						int type = -1;
						if (typeName != null)
						{
							if (typeName.equals("short text") || typeName.equals("history text"))
							{
								type = ShortText;
							}
							else if (typeName.equals("single list"))
							{
								_listFields.add(id);
								type = ShortText;
							}							
							else if (typeName.endsWith("list"))
							{
								int colNumber = 2;
								if (typeName.equals("triple list"))
								{
									colNumber = 3;
								}
								String[] labels = new String[colNumber];
								labels[0] = getLabel(xpp.getAttributeValue(null, "label1"));
								labels[1] = getLabel(xpp.getAttributeValue(null, "label2"));
								if (colNumber > 2)
								{
									labels[2] = getLabel(xpp.getAttributeValue(null, "label3"));
								}
								_listFields.add(id);
								type = MultiList;
								addField(group, id, label, labels, type);
								alreadyAdded = true;
							}							
							else if (typeName.equals("number"))
							{
								type = ShortText;
							}
							else if (typeName.equals("date"))
							{
								type = Date;
							}
							else if (typeName.equals("long text"))
							{
								type = LongText;
							}
							else if (typeName.equals("image"))
							{
								if (id.equals(_coverField))
								{
									type = Cover;
								}
								else
								{
									type = Image;
									group = res.getString(R.string.images);
								}
							}
							else if (typeName.equals("button"))
							{
								if (id.equals(_urlField))
								{
									type = Url;
								}
							}
							else if (typeName.equals("yesno"))
							{
								type = YesNo;
							}
						}
						if (!alreadyAdded && (type != -1) && gotTitle)
						{
							addField(group, id, label, type);
						}
						if (_titleField.equals(id))
						{
							gotTitle = true;
						}
					}
				}
				else if (eventType == XmlPullParser.TEXT)
				{
					if (isTitle)
					{
						_titleField = xpp.getText();
						isTitle = false;
					}
					else if (isCover)
					{
						_coverField = xpp.getText();
						isCover = false;
					}
					else if (isUrl)
					{
						_urlField = xpp.getText();
						isUrl = false;
					}
				}
				eventType = xpp.next();
			}
		}
		catch (XmlPullParserException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		addExtras();
	}
	
	public void addExtras()
	{
		if (_hasLending)
		{
			String groupName = getLabel("PanelLending");
			
			addField(groupName, "borrower", getLabel("PanelBorrower"), GCstarModel.ShortText);
			addField(groupName, "lendDate", getLabel("PanelLendDate"), GCstarModel.ShortText);
			String[] historyLabels = {getLabel("PanelBorrower"),getLabel("PanelLendDate"),getLabel("PanelReturnDate")};
			addField(groupName, "borrowings", getLabel("PanelHistory"), historyLabels, GCstarModel.MultiList);
			_listFields.add("borrowings");
		}
		if (_hasTags)
		{
			addField(_groups.get(0), "tags", getLabel("PanelTags"), GCstarModel.ShortText);
			_listFields.add("tags");
		}
	}
	
	public ArrayList<String> getGroups()
	{
		return _groups;
	}
	
	public ArrayList<GCstarField> getFields(String group)
	{
		return _fields.get(group);
	}
	
	public int getFieldType(String field)
	{
		if (_fieldTypes.containsKey(field))
		{
			return _fieldTypes.get(field);
		}
		else
		{
			return None;
		}
	}
	
	public String getTitleField()
	{
		return _titleField;
	}
	
	public String getCoverField()
	{
		return _coverField;
	}
	
	public String getUrlField()
	{
		return _urlField;
	}
	
	public boolean isList(String id)
	{
		return _listFields.contains(id);
	}
	
    public String getLabel(String label)
	{
		if (label == null)
		{
			return label;
		}
		Resources res = _activity.getResources();
		String prefix = new String((label.startsWith("Panel") || (label.startsWith("Context")))
									? "generic" : _name);
		int xmlId = res.getIdentifier(prefix + "." + label, "string", "com.gcstar.viewer");
		if (xmlId == 0)
		{
			return label;
		}
		else
		{
			return res.getString(xmlId);
		}
	}
	
	protected void addField(String group, String field, String label, int type)
	{
		checkGroup(group);
		_fields.get(group).add(new GCstarField(field, label, type));
		_fieldTypes.put(field, type);
	}
	
	protected void addField(String group, String field, String label, String[] labels, int type)
	{
		checkGroup(group);
		_fields.get(group).add(new GCstarField(field, label, labels, type));
		_fieldTypes.put(field, type);
	}
	
	protected void checkGroup(String group)
	{
		if (_groups.indexOf(group) == -1)
		{
			if (group.equals(_activity.getResources().getString(R.string.images)))
			{
				_groups.add(group);				
			}
			else
			{
				int position = _groups.size() - 1;
				_groups.add((position > 0) ? position : 0, group);
			}
			_fields.put(group, new ArrayList<GCstarField>());		
		}
	}
	
	protected String getGroupName(String group, int type)
	{
		String groupName = new String(group);
		return String.format("%03d%s", type, groupName);
	}
	
	private String _name;
	private String _titleField;
	private String _coverField;
	private String _urlField;
	private GCstarViewer _activity;
	private ArrayList<String> _groups;
	//private ArrayList<String> _longFields;
	private HashMap<String, ArrayList<GCstarField>> _fields;
	private HashMap<String, Integer> _fieldTypes;
	private HashSet<String> _listFields;
	private boolean _hasLending;
	private boolean _hasTags;
}
