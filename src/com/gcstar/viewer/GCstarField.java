/****************************************************
 *
 *  Copyright 2010 Christian Jodar
 *
 *  This file is part of GCstar Viewer.
 *
 *  GCstar Viewer is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  GCstar Viewer is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GCstar Viewer; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************/

package com.gcstar.viewer;

public class GCstarField
{
    public GCstarField(String name, String label, int type)
    {
        _name = name;
        _label = label;
        _type = type;
        _columns = 0;
    }

    public GCstarField(String name, String label, String[] labels, int type)
    {
        _name = name;
        _label = label;
        _type = type;
        _labels = labels;
        _columns = _labels.length;
    }

    public String _name;
    public String _label;
    public int _type;
    public String[] _labels;
    public int _columns;
};
