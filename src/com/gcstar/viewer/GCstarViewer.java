/****************************************************
 *
 *  Copyright 2010 Christian Jodar
 *
 *  This file is part of GCstar Viewer.
 *
 *  GCstar Viewer is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  GCstar Viewer is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GCstar Viewer; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************/

package com.gcstar.viewer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Configuration;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.SearchRecentSuggestions;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class GCstarViewer extends Activity implements OnClickListener,
        OnItemClickListener
{

    public class ImageAdapter extends BaseAdapter
    {
        public ImageAdapter(Context c)
        {
            _context = c;
        }

        public int getCount()
        {
            return _items.size();
        }

        public Object getItem(int position)
        {
            return null;
        }

        public long getItemId(int position)
        {
            return 0;
        }

        // create a new ImageView for each item referenced by the Adapter
        public View getView(int position, View convertView, ViewGroup parent)
        {
            ImageView imageView;
            if (convertView == null)
            { // if it's not recycled, initialize some attributes
                imageView = new ImageView(_context);
                imageView.setLayoutParams(new GridView.LayoutParams(_imageHeight, _imageHeight));
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageView.setPadding(8, 8, 8, 8);
            }
            else
            {
                imageView = (ImageView) convertView;
            }
            imageView.setImageResource(android.R.drawable.ic_popup_sync);

            // imageView.setImageResource(mThumbIds[position]);
            // GCstarViewer _parent = GCstarViewer.this;
            GCstarViewer _parent = (GCstarViewer) _context;
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 4;
            _loader.loadImage(imageView, _parent._items.get(position)
                    .getCover(), true, /* isCover */
                    _imageHeight, /* image height */
            options, true); /* put in cache */
            return imageView;
        }

        private Context _context;
    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        _loader = null;

        _dialog = new GCstarItemDialog(this);
        _collectionsDialog = new GCstarCollectionsChooser(this);
        _currentFilter = new String("");

        // Try to get filename, otherwise, open file chooser
        SharedPreferences settings = getPreferences(Context.MODE_PRIVATE);

        boolean isList = settings.getBoolean("isList", true);
        setViewType(isList);
        resetImageHeight();
        String fileName = settings.getString("fileName", "");
        if (fileName.length() == 0)
        {
            chooseFile();
        }
        else
        {
            loadFile(fileName);
        }
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        if (_loader != null)
        {
            _loader.cleanup();
        }
    }

    @Override
    protected void onNewIntent(Intent intent)
    {
        setIntent(intent);
        String query = intent.getStringExtra(SearchManager.QUERY);
        filterItems(query);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK)
                && (_currentFilter.length() != 0))
        {
            filterItems("");
            return true;
        }
        else
        {
            return super.onKeyDown(keyCode, event);
        }
    }

    public void resetImageHeight()
    {
        _imageHeight = _dialog._imageHeight / 2;
        if (_imageHeight < 80)
        {
            _imageHeight = 80;
        }
    }

    public void setViewType(boolean isList)
    {
        _isList = isList;
        if (isList)
        {
            //if (_groupField.length() > 0)
            //{
            //    _listView = new ExpandableListView(this);
            //}
            _listView = new ListView(this);
            _listView.setTextFilterEnabled(false);
            _listView.setOnItemClickListener(this);
            setContentView(_listView);
        }
        else
        {
            setContentView(R.layout.grid);

            _gridView = (GridView) findViewById(R.id.gridview);

            _gridView.setOnItemClickListener(new OnItemClickListener()
            {
                public void onItemClick(AdapterView<?> parent, View v,
                        int position, long id)
                {
                    showItem(position);
                    showDialog(0);
                }
            });
        }
    }

    public void showItem(int index)
    {
        _currentItemIndex = index;
        _dialog.setCurrentItem(_items.get(_currentItemIndex));
    }

    public void showRelativeItem(int index)
    {
        if (index < 0)
        {
            _currentItemIndex--;
        }
        else
        {
            _currentItemIndex++;
        }
        _dialog.setCurrentItem(_items.get(_currentItemIndex));
    }

    public boolean[] areRelativesPossible()
    {
        boolean[] prevNext = { false, false };
        if (_currentItemIndex > 0)
        {
            prevNext[0] = true;
        }
        if (_currentItemIndex < (_items.size() - 1))
        {
            prevNext[1] = true;
        }
        return prevNext;
    }

    public void chooseFile()
    {
        Intent intent = new Intent(this, GCstarFileChooser.class);
        startActivityForResult(intent, 0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle item selection
        switch (item.getItemId())
        {
        case R.id.open:
            chooseFile();
            return true;
        case R.id.invert:
            if (_loader != null)
            {
                _loader.reverseOrder();
                if (_currentFilter.length() != 0)
                {
                    filterItems(_currentFilter);
                }
            }
            return true;
        case R.id.search:
            onSearchRequested();
            return true;
            /*
             * case R.id.showall: filterItems(""); return true;
             */
        case R.id.collections:
            showDialog(1);
            return true;
        case R.id.changeview:
            _isList = !_isList;
            setViewType(_isList);
            String notif;
            if (_isList)
            {
                notif = getResources().getString(R.string.textmode);
            }
            else
            {
                notif = getResources().getString(R.string.imgmode);
            }
            Toast.makeText(this, notif, Toast.LENGTH_SHORT).show();
            filterItems("");
            SharedPreferences settings = getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = settings.edit();
            editor.putBoolean("isList", _isList);
            editor.commit();
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }

    /* LIST VIEW */
    // protected void onListItemClick(ListView l, View v, int position, long id)
    public void onItemClick(AdapterView<?> parent, View v, int position, long id)
    {
        // super.onListItemClick(l, v, position, id);

        showItem(position);
        showDialog(0);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (resultCode == RESULT_OK)
        {
            String fileName = data.getStringExtra("file");
            if (fileName != null)
            {
                loadFile(fileName);
            }
        }
    }

    protected Dialog onCreateDialog(int id)
    {
        if (id == 0)
        {
            return _dialog;
        }
        else if (id == 1)
        {
            return _collectionsDialog;
        }
        return null;
    }

    public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
        _dialog.resetImageHeight();
        resetImageHeight();
    }

    public void onClick(DialogInterface arg0, int button)
    {
        if (button == AlertDialog.BUTTON_POSITIVE)
        {
            String textString = _filterTextInput.getText().toString().trim();
            filterItems(textString);
        }
    }

    protected void loadFile(String fileName)
    {
        _loader = new GCstarCollectionLoader(this);
        _loader.load(fileName);
    }

    public class SuggestionsTask extends
            AsyncTask<GCstarViewer, Integer, Integer>
    {
        protected Integer doInBackground(GCstarViewer... p)
        {
            _parent = p[0];
            ArrayList<String> wordList = new ArrayList<String>();
            SearchRecentSuggestions suggestions = new SearchRecentSuggestions(
                    _parent, GCstarSuggestions.AUTHORITY,
                    GCstarSuggestions.MODE);
            suggestions.clearHistory();
            Pattern pattern = Pattern.compile("\\W");

            for (String item : _parent._itemsNames)
            {
                String[] words = pattern.split(item);
                for (String word : words)
                {
                    if (word.length() > 2)
                    {
                        wordList.add(word);
                    }
                }
            }
            Collections.sort(wordList, new java.util.Comparator<String>()
            {
                public int compare(String a, String b)
                {
                    return b.compareToIgnoreCase(a);
                }
            });
            for (String word : wordList)
            {
                suggestions.saveRecentQuery(word, null);
            }
            return 0;
        }

        protected void onPostExecute(Integer result)
        {
        }

        protected GCstarViewer _parent;
    };

    public void setItems(ArrayList<GCstarItem> itemsList, String fileName)
    {
        _items = itemsList;

        _itemsNames = new String[_items.size()];
        int i = 0;
        for (GCstarItem item : _items)
        {
            _itemsNames[i] = item.getTitle();
            i++;
        }
        if (_isList)
        {
            /* LIST VIEW */
            //if (_groupField.length() > 0)
            //{
            //}

            _listView.setAdapter(new ArrayAdapter<String>(this,
                    R.layout.list_item, _itemsNames));
            /*
             * setListAdapter(new ArrayAdapter<String>(this, R.layout.list_item,
             * items));
             */
        }
        else
        {
            _gridView.setAdapter(new ImageAdapter(this));
        }

        if (fileName != null)
        {
            SharedPreferences settings = getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = settings.edit();
            editor.putString("fileName", fileName);
            editor.commit();

            new SuggestionsTask().execute(this);
        }
    }

    public void setModel(GCstarModel model)
    {
        _dialog.setModel(model);
    }

    public void filterItems(String filter)
    {
        _currentFilter = filter;
        if (_loader == null)
        {
            return;
        }
        ArrayList<GCstarItem> items;
        if (filter.equals(""))
        {
            items = _loader.getItems();
        }
        else
        {
            items = new ArrayList<GCstarItem>();
            String lcfilter = filter.toLowerCase();
            for (GCstarItem item : _loader.getItems())
            {
                if (item.getTitle().toLowerCase().contains(lcfilter))
                {
                    items.add(item);
                }
            }
        }
        setItems(items, null);
    }

    public String getFileName()
    {
        return _loader.getFileName();
    }

    public GCstarCollectionLoader getLoader()
    {
        return _loader;
    }

    private ArrayList<GCstarItem> _items;
    private GCstarItemDialog _dialog;
    private GCstarCollectionsChooser _collectionsDialog;
    private GCstarCollectionLoader _loader;
    private EditText _filterTextInput;
    private String _currentFilter;
    private GridView _gridView;
    private ListView _listView;
    private boolean _isList;
    //private String _groupField = "genre";
    private int _currentItemIndex;
    public String[] _itemsNames;
    public int _imageHeight;
};
