/****************************************************
 *
 *  Copyright 2010 Christian Jodar
 *
 *  This file is part of GCstar Viewer.
 *
 *  GCstar Viewer is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  GCstar Viewer is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GCstar Viewer; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************/

package com.gcstar.viewer;

import java.io.File;
import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class GCstarFileChooser extends ListActivity
{

    private class LoadFilesTask extends
            AsyncTask<GCstarFileChooser, Integer, Integer>
    {
        protected void getCollections(File dir)
        {
            File[] files = dir.listFiles();
            if (files != null)
            {
                for (File f : files)
                {
                    if (f.isDirectory())
                    {
                        getCollections(f);
                    }
                    else
                    {
                        if ((f.getName().endsWith(".gcs"))
                                || (f.getName().endsWith(".gcz")))
                        {
                            _parent._collections.add(f);
                        }
                    }
                }
            }
        }

        protected Integer doInBackground(GCstarFileChooser... p)
        {
            _parent = p[0];
            getCollections(Environment.getExternalStorageDirectory());

            return 1;
        };

        protected void onPostExecute(Integer result)
        {
            String files[] = new String[_parent._collections.size()];
            int i = 0;
            for (File f : _parent._collections)
            {
                files[i] = f.getName();
                i++;
            }
            setListAdapter(new ArrayAdapter<String>(_parent,
                    R.layout.list_item, files));
            ListView lv = _parent.getListView();
            lv.setTextFilterEnabled(true);
            _parent.stopDialog();
            if (i == 0)
            {
                _parent.warning();
            }
        }

        protected GCstarFileChooser _parent;
    };

    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        _collections = new ArrayList<File>();

        _dialog = ProgressDialog.show(this, "", getResources().getString(
                R.string.loading), true);
        new LoadFilesTask().execute(this);
    }

    protected void onListItemClick(ListView l, View v, int position, long id)
    {
        super.onListItemClick(l, v, position, id);
        Intent result = new Intent();
        String fileName = _collections.get(position).getAbsolutePath();

        result.putExtra("file", fileName);
        setResult(RESULT_OK, result);
        finish();
    }

    public void stopDialog()
    {
        _dialog.dismiss();
    }

    public void warning()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getResources().getString(R.string.warning))
                .setCancelable(false)
                .setPositiveButton(
                    getResources().getString(R.string.moreinfo),
                        new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                Intent intent = new Intent();
                                intent.setAction(android.content.Intent.ACTION_VIEW);
                                intent.setData(Uri.parse("http://wiki.gcstar.org/en/gcstar_viewer"));
                                startActivity(intent);
                                dialog.dismiss();
                            }
                        })
                .setNegativeButton(
                    getResources().getString(R.string.cancel),
                        new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private ArrayList<File> _collections;
    private ProgressDialog _dialog;
}
