package com.gcstar.viewer;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;

public class GCstarImageLoader
{
    public GCstarImageLoader(Context context, GCstarCollectionLoader loader)
    {
        _imageLoaderThread = new ImagesLoader(context);
        // Make the background thead low priority. This way it will not affect
        // the UI performance
        _imageLoaderThread.setPriority(Thread.NORM_PRIORITY - 1);
        _loader = loader;
    }

    public void displayImage(String path, Activity activity,
            ImageView imageView, int height, boolean isCover, boolean cache,
            Options options)
    {
        ImageToLoad img = null;
        if (cache)
        {
            img = _cache.get(path);
        }
        if ((img != null) && (img._bm != null) && (!img._bm.isRecycled()))
        {
            img._imageView = imageView;
            GCstarImageLoader.SetImage(img);
        }
        else
        {
            // This ImageView may be used for other images before. So there may
            // be some old tasks in the queue. We need to discard them.
            _imagesQueue.Clean(imageView);
            ImageToLoad p = new ImageToLoad(path, imageView, height, isCover,
                    cache, options);
            synchronized (_imagesQueue._imagesList)
            {
                _imagesQueue._imagesList.offer(p);
                _imagesQueue._imagesList.notifyAll();
            }

            // start thread if it's not started yet
            if (_imageLoaderThread.getState() == Thread.State.NEW)
                _imageLoaderThread.start();
            imageView.setImageResource(defaultId);
        }
    }

    private void getBitmap(ImageToLoad img)
    {
        String path = _loader.convertImagePath(img._path);
        File file = new File(path);
        if (!file.isAbsolute())
        {
            file = new File(_loader.getCurrentDir(), path);
        }
        img._bm = null;
        try
        {
            if (file.exists() && file.isFile())
            {
                img._bm = BitmapFactory.decodeFile(file.getAbsolutePath(), img._options);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        Uri uri = null;
        if (img._bm != null)
        {
            uri = Uri.fromFile(file);
            img._desc = uri.toString();
        }
    }

    // Task for the queue
    private class ImageToLoad
    {
        public ImageToLoad(String path, ImageView i, int height,
                boolean isCover, boolean cache, Options options)
        {
            _path = path;
            _imageView = i;
            _height = height;
            _isCover = isCover;
            _options = options;
            _cache = cache;
        }

        public String _path;
        public ImageView _imageView;
        public int _height;
        public boolean _isCover;
        public Options _options;
        public Bitmap _bm;
        public String _desc;
        public boolean _cache;
    }

    public void stopThread()
    {
        _imageLoaderThread.interrupt();
    }

    class ImagesQueue
    {

        // removes all instances of this ImageView
        public void Clean(ImageView image)
        {
            for (int j = 0; j < _imagesList.size();)
            {
                if (_imagesList.get(j)._imageView == image)
                {
                    _imagesList.remove(j);
                }
                else
                {
                    ++j;
                }
            }
        }

        private LinkedList<ImageToLoad> _imagesList = new LinkedList<ImageToLoad>();
    }

    class ImagesLoader extends Thread
    {
        public ImagesLoader(Context c)
        {
            _context = c;
        }

        public void run()
        {
            try
            {
                while (true)
                {
                    // thread waits until there are any images to load in the
                    // queue
                    if (_imagesQueue._imagesList.size() == 0)
                    {
                        synchronized (_imagesQueue._imagesList)
                        {
                            _imagesQueue._imagesList.wait();
                        }
                    }
                    if (_imagesQueue._imagesList.size() != 0)
                    {
                        ImageToLoad imageToLoad;
                        synchronized (_imagesQueue._imagesList)
                        {
                            imageToLoad = _imagesQueue._imagesList.poll();
                        }
                        getBitmap(imageToLoad);
                        if (imageToLoad._cache)
                        {
                            _cache.put(imageToLoad._path, imageToLoad);
                        }
                        if (((String) imageToLoad._imageView.getTag())
                                .equals(imageToLoad._path))
                        {
                            BitmapDisplayer bd = new BitmapDisplayer(
                                    imageToLoad);
                            Activity a = (Activity) _context;
                            a.runOnUiThread(bd);
                        }
                    }
                    if (Thread.interrupted())
                    {
                        break;
                    }
                }
            }
            catch (InterruptedException e)
            {
                // allow thread to exit
            }
        }

        private Context _context;
    }

    public static void SetImage(ImageToLoad img)
    {
        if (img._bm != null)
        {
            img._imageView.setImageBitmap(img._bm);
            img._imageView.setContentDescription(img._desc);
            img._imageView.setVisibility(View.VISIBLE);
            img._imageView.setMaxHeight(img._height);
            img._imageView.getLayoutParams().height = img._isCover ? img._height
                    : LayoutParams.WRAP_CONTENT;
        }
        else
        {
            img._imageView.setImageResource(defaultId);
        }

    }

    // Used to display bitmap in the UI thread
    class BitmapDisplayer implements Runnable
    {
        public BitmapDisplayer(ImageToLoad img)
        {
            _img = img;
        }

        public void run()
        {
            GCstarImageLoader.SetImage(_img);
        }

        private ImageToLoad _img;
    }

    public void clearCache()
    {
        for (ImageToLoad img : _cache.values())
        {
            if (img._bm != null)
            {
                img._bm.recycle();
            }
        }
        _cache.clear();
    }

    // \TODO This can be replaced with SoftReference or
    // BitmapOptions.inPurgeable
    private HashMap<String, ImageToLoad> _cache = new HashMap<String, ImageToLoad>();
    private GCstarCollectionLoader _loader;
    private ImagesQueue _imagesQueue = new ImagesQueue();
    private ImagesLoader _imageLoaderThread;

    static final int defaultId = R.drawable.no_pic;

}
