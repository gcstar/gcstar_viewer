/****************************************************
 *
 *  Copyright 2010 Christian Jodar
 *
 *  This file is part of GCstar Viewer.
 *
 *  GCstar Viewer is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  GCstar Viewer is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GCstar Viewer; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************/

package com.gcstar.viewer;

import java.util.ArrayList;
import java.util.Collections;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class GCstarCollectionsChooser extends Dialog implements
        OnClickListener, OnItemClickListener
{
    private class Collection
    {
        public Collection(String name, String path)
        {
            _name = name;
            _path = path;
        }

        public String _name;
        public String _path;
    }

    private class CollectionSorter implements java.util.Comparator<Collection>
    {
        public CollectionSorter()
        {
        }

        public int compare(Collection a, Collection b)
        {
            return a._name.compareToIgnoreCase(b._name);
        }
    };

    public GCstarCollectionsChooser(Context context)
    {
        super(context);// , android.R.style.Theme);
        _parent = (GCstarViewer) context;
        _collections = new ArrayList<Collection>();
        setTitle(R.string.menucollections);
    }

    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        LinearLayout layout = new LinearLayout(getContext());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.FILL_PARENT,
                LinearLayout.LayoutParams.FILL_PARENT);
        layout.setLayoutParams(params);
        layout.setOrientation(LinearLayout.VERTICAL);

        Button b = new Button(getContext());
        b.setText(R.string.addcollection);
        b.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                _nameDialog = new AlertDialog.Builder(_parent);
                _nameDialog.setTitle(R.string.addcollection);
                _nameDialog.setMessage(R.string.collectionname);
                _nameText = new EditText(_parent);
                _nameDialog.setView(_nameText);
                _nameDialog.setPositiveButton(R.string.ok,
                        GCstarCollectionsChooser.this);
                _nameDialog.setNeutralButton(R.string.cancel,
                        GCstarCollectionsChooser.this);
                _nameText.setText(_parent.getLoader().getCollectionName());
                _nameDialog.show();
            }
        });

        _list = new ListView(getContext());
        ListView.LayoutParams lparams = new ListView.LayoutParams(
                ListView.LayoutParams.FILL_PARENT,
                ListView.LayoutParams.FILL_PARENT);
        _list.setLayoutParams(lparams);
        _list.setLongClickable(true);
        this.registerForContextMenu(_list);
        _list.setOnItemClickListener(this);

        layout.addView(b);
        layout.addView(_list);

        setContentView(layout);

        load();
        refreshView();
    }

    public void onItemClick(AdapterView<?> adapter, View v, int position,
            long id)
    {
        dismiss();
        _parent.loadFile(_collections.get(position)._path);
    }

    public void onCreateContextMenu(ContextMenu menu, View v,
            ContextMenuInfo menuInfo)
    {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        _selectedItem = (int) info.id;
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderIcon(android.R.drawable.ic_menu_more);
        menu.setHeaderTitle(_collections.get(_selectedItem)._name);
        menu.add(0, 0, 0, R.string.renamecollection);
        menu.add(0, 1, 0, R.string.deletecollection);

    }

    public boolean onMenuItemSelected(int i, MenuItem item)
    {
        // AdapterContextMenuInfo info = (AdapterContextMenuInfo)
        // item.getMenuInfo();
        switch (item.getItemId())
        {
        // rename
        case 0:
            _renaming = true;
            _nameDialog = new AlertDialog.Builder(_parent);
            _nameDialog.setTitle(R.string.renamecollection);
            _nameDialog.setMessage(R.string.collectionname);
            _nameText = new EditText(_parent);
            _nameDialog.setView(_nameText);
            _nameDialog.setPositiveButton(R.string.ok, this);
            _nameDialog.setNeutralButton(R.string.cancel, this);
            _nameText.setText(_collections.get(_selectedItem)._name);
            _nameDialog.show();
            // _renaming = false;
            return true;
            // delete
        case 1:
            _collections.remove(_selectedItem);
            refreshView();
            save();
            return true;
        }
        return super.onMenuItemSelected(i, item);
    }

    public void onClick(DialogInterface dialog, int button)
    {
        if (button == AlertDialog.BUTTON_POSITIVE)
        {
            String name = _nameText.getText().toString().trim();
            if (_renaming)
            {
                _collections.get(_selectedItem)._name = name;
                _renaming = false;
            }
            else
            {
                String path = _parent.getFileName();
                _collections.add(new Collection(name, path));
            }
            refreshView();
            save();
        }
    }

    public void refreshView()
    {
        // _list.removeAllViews();
        Collections.sort(_collections, new CollectionSorter());

        String[] collections = new String[_collections.size()];
        int i = 0;
        for (Collection col : _collections)
        {
            collections[i] = col._name;
            i++;
        }
        _list.setAdapter(new ArrayAdapter<String>(getContext(),
                R.layout.list_item, collections));
    }

    public void save()
    {
        SharedPreferences settings = _parent
                .getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        int i = 0;
        for (Collection col : _collections)
        {
            editor.putString(i + "_name", col._name);
            editor.putString(i + "_path", col._path);
            i++;
        }
        editor.putInt("nbcol", i);
        editor.commit();

    }

    public void load()
    {
        SharedPreferences settings = _parent
                .getPreferences(Context.MODE_PRIVATE);
        int nbcol = settings.getInt("nbcol", 0);
        _collections.clear();
        for (int i = 0; i < nbcol; i++)
        {
            Collection col = new Collection(
                    settings.getString(i + "_name", ""), settings.getString(i
                            + "_path", ""));
            _collections.add(col);
        }
    }

    private GCstarViewer _parent;
    private ListView _list;
    private ArrayList<Collection> _collections;
    private AlertDialog.Builder _nameDialog;
    private EditText _nameText;
    private int _selectedItem;
    private boolean _renaming;
};
